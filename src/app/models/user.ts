import { Role } from "./role";
import { Pole } from "./pole";
import { Project } from "./project";

export class User {
    _id: string;
    firstname: string;
    lastname: string;
    email: string;
    password: string;
    role: Role;
    pole: Pole = new Pole();
    project: Project = new Project();
}
