export class Feedback {
    _id: string;
    feeling?: number;
    successTasks?: string;
    difficulties?: string;
}
