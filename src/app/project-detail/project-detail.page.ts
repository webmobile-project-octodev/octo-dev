import { Component, OnInit } from "@angular/core";
import { ModalController, NavParams } from "@ionic/angular";
import { Project } from "../models/project";

@Component({
  selector: "app-project-detail",
  templateUrl: "./project-detail.page.html",
  styleUrls: ["./project-detail.page.scss"]
})
export class ProjectDetailPage implements OnInit {
  randomPicture: String;
  project: Project;
  isLoading = true;

  constructor(
    private modalController: ModalController,
    private navParams: NavParams
  ) {}

  ngOnInit() {
    let random = Math.floor(Math.random() * (200 - 190 + 1) + 190);
    this.randomPicture = "https://picsum.photos/" + random + "/150/?random";
    this.project = this.navParams.get("project");

    setTimeout(() => {
      this.isLoading = false;
    }, 1000);
  }

  closeModal() {
    this.modalController.dismiss();
  }
}
