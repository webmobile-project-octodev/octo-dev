import { Component, OnInit } from '@angular/core';
import { NavParams, ModalController } from '@ionic/angular';
import { ProjectService } from '../services/project.service';
import { Project } from '../models/project';
import { PoleService } from '../services/pole.service';
import { Pole } from '../models/pole';
import { ProjectDetailPage } from '../project-detail/project-detail.page';

@Component({
  selector: 'app-modal-page',
  templateUrl: './modal-page.page.html',
  styleUrls: ['./modal-page.page.scss'],
})
export class ModalPagePage implements OnInit {

  poleId: number;
  projectList: Project[];
  projectsFilter: Project[];
  pole: Pole;

  constructor(private navParams: NavParams, private modalController: ModalController, 
    private projectService: ProjectService, private poleService: PoleService) { 
  }

  ngOnInit() {
    this.poleId = this.navParams.get('pole_id');
    this.projectService.getProjectsByPole(this.poleId).subscribe(
      projects => {
        this.projectList=projects;
        this.projectsFilter = projects;
      },
      err => console.log(err)
    )

    this.poleService.getPoleById(this.poleId).subscribe(
      pole => this.pole = pole,
      err => console.log(err)
    )
  }

  async presentModal(project) {
    const modal = await this.modalController.create({
      component: ProjectDetailPage,
      componentProps: { project: project }
    });
    modal.present();
  }

  filterProjects(ev: any) {
    let val = ev.target.value;

    if (val == ""){
      this.projectsFilter = this.projectList;
    } else {
      if (val && val.trim() !== '') {
      this.projectsFilter = this.projectList.filter(function(project) {
        return project.name.toLowerCase().startsWith(val.toLowerCase());
      });
      }
    }
  }

  closeModal() {
    this.modalController.dismiss();
  }

}
