import { Component, OnInit } from '@angular/core';
import { ModalController, NavController } from '@ionic/angular';
import { ModalPagePage } from '../modal-page/modal-page.page';
import { PoleService } from '../services/pole.service';
import { ProjectService } from '../services/project.service';
import { Pole } from '../models/pole';
import { Project } from '../models/project';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page implements OnInit {

  polesSubscription: Subscription;
  projectsSubscription: Subscription;

  polesList: Pole[];
  polesFilter: Pole[];
  projects: Project[];
  id: number;
  

    constructor(private modalController: ModalController, private nav: NavController, private poleService: PoleService,
      private projectService: ProjectService) {}

    ngOnInit() {
      this.polesSubscription = this.poleService.getPoles().subscribe(poles => {
        this.polesList = poles;    
        this.polesFilter = this.polesList;
      })
      this.projectsSubscription = this.projectService.getProjects().subscribe(projects => {
        this.projects = projects
      })
    }

    async presentModal(pole_id) {
      const modal = await this.modalController.create({
        component: ModalPagePage,
        componentProps: { pole_id: pole_id }
      });
      modal.present();
    }

    filterPoles(ev: any) {
      let val = ev.target.value;
      if (val == ""){
        this.polesFilter = this.polesList;
      } else {
        if (val && val.trim() !== '') {      
        this.polesFilter = this.polesList.filter(function(pole) {
          return pole.name.toLowerCase().startsWith(val.toLowerCase());
        });
        }
      }
    }
}
