import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';
import { UserService } from '../services/user.service';
import { User } from '../models/user';
import { Router } from '@angular/router';
import { ToastController } from '@ionic/angular';
import { AlertController } from '@ionic/angular';
import { checkAndUpdateElementDynamic } from '@angular/core/src/view/element';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss']
})
export class RegisterPage implements OnInit {

  connexion = true;

  errRegisterPassword = false;
  errRegisterEmail = false;
  errRegisterLastName = false;
  errRegisterFirstName = false;
  formNotValid = false;

  user: User;
  isLoading = true;

  stylePassword: any;
  styleEmail: any;

  constructor(private userService: UserService, private router: Router,
     public toastController: ToastController, private storage: Storage) { 
      this.stylePassword = "ng-valid",
      this.styleEmail = "ng-valid"
     }

  ngOnInit() {
    this.user = new User();
    this.storage.get('userId').then((userId) => {
      console.log(userId);
      if (userId !== null) {
        this.router.navigate(['/tabs']);
      } else {
        this.isLoading = false;
        this.user = new User();
      }
    });
  }

  //Lors du clic sur Inscription
  checkForm() {

    this.formNotValid = false;
    this.errRegisterLastName = false;
    this.errRegisterFirstName = false;

    if (this.checkEmail()) {
      this.formNotValid = true;
    }

    if (this.checkPassword()) {
      this.formNotValid = true;
    }


    //Si tout est rempli et que le mail et password sont valides, on lance l'inscription
    if (this.user.firstname && this.user.lastname && !this.formNotValid) {
      this.save();
    } else {

      //Sinon, on regarde ce qui est vide pour afficher le message d'erreur
      if (!this.user.lastname) {
        this.errRegisterLastName = true;
        this.formNotValid = true;
      } else {
        this.errRegisterLastName = false;
      }

      if (!this.user.firstname) {
        this.errRegisterFirstName = true;
        this.formNotValid = true;
      } else {
        this.errRegisterFirstName = false;
      }
    }
  }


  save() {

    this.userService.createUser(this.user).subscribe(
      res => {
        console.log(res);
        const response = JSON.parse(res);
        this.presentToast(response.message, 'success');
        this.connexion = true;
      },
      err => {
        console.log(err)
        const error = JSON.parse(err.error);     
        if(err.status === 400) {
          this.presentToast(error.message, 'danger');
        }
      }
    )
  }

  login() {    
    this.userService.authentication(this.user).subscribe(
      res => {  
        if(res) {
          console.log(JSON.parse(res));
          this.storage.set('userId', JSON.parse(res)._id).then(() => {
            this.router.navigate(['/tabs']);
          });
        }
      },
      err => {
        this.presentToast(JSON.parse(err.error).message, 'danger');
      }
    )
  }

  checkEmail(): boolean {

    const regex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

    if (regex.test(this.user.email) === false) {

      this.styleEmail = "ng-invalid"
      // on indique qu'il y a une erreur
      this.errRegisterEmail = true;
    }
    else {
      this.styleEmail = "ng-valid"
      this.errRegisterEmail = false;
    }
    return this.errRegisterEmail;

  }

  checkPassword(): boolean {

    const regex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[-_@$!%*?&])[A-Za-z\d-_@$!%*?&]{8,}$/;



    if (regex.test(this.user.password) === false) {

      this.stylePassword = "ng-invalid"
      
      this.errRegisterPassword = true;
    }
    else {
      this.stylePassword = "ng-valid"
      // on indique qu'il y a une erreur
      this.errRegisterPassword = false;
    }
    return this.errRegisterPassword;
  }

  async presentToast(message, status) {
    const toast = await this.toastController.create({
      message: message,
      duration: 3000,
      position: "top",
      color: status
    });
    toast.present();
  }

  isConnexion() {
    this.connexion = !this.connexion;
    this.stylePassword = "ng-valid";
  }

}
