import { Component, OnInit, OnDestroy } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { FeedbackService } from '../services/feedback.service';
import { Subscription } from 'rxjs';
import { Feedback } from '../models/feedback';
import { FeedbackPage } from '../feedback/feedback.page';

@Component({
  selector: 'app-feedback-list',
  templateUrl: './feedback-list.page.html',
  styleUrls: ['./feedback-list.page.scss'],
})
export class FeedbackListPage implements OnInit, OnDestroy {

  feedbacksSubscription: Subscription;
  feedbacks: Feedback[];
  messages: any[] = [];
  subscription: Subscription;

  constructor(private modalController: ModalController, private feedbackService: FeedbackService) { }

  ngOnInit() {
    this.feedbacksSubscription = this.feedbackService.getFeedbacks().subscribe(feedbacks => {
      this.feedbackService.sendFeedback(feedbacks);
    });

    this.subscription = this.feedbackService.getFeedbacksSubject().subscribe(feedbacks => {
      this.feedbacks = feedbacks;
      console.log(feedbacks);
    });
  }

  ngOnDestroy(): void {
    this.feedbacksSubscription.unsubscribe();
  }

  async presentModal() {
    const modal = await this.modalController.create({
      component: FeedbackPage
    });
    modal.present();
  }
}
