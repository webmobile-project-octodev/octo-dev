import { Component } from '@angular/core';
import { User } from '../models/user';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {

  user: User;

  constructor(private userService: UserService) {
    this.user = new User();
  }

  connect() {
    this.userService.authentication(this.user);
  }
  
}
