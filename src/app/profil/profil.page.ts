import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';
import { User } from '../models/user';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { Pole } from '../models/pole';
import { PoleService } from '../services/pole.service';
import { Project } from '../models/project';
import { ProjectService } from '../services/project.service';
import { UserService } from '../services/user.service';
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-profil',
  templateUrl: './profil.page.html',
  styleUrls: ['./profil.page.scss'],
})

export class ProfilPage implements OnInit {

  user: User;

  polesSubscription: Subscription;
  rolesSubscription: Subscription;
  projectsSubscription: Subscription;

  poles: Pole[];
  projects: Project[];
  projectsFilter: Project[];

  edit = true;
  isLoading = true;

  constructor(private storage: Storage, private router: Router, private poleService: PoleService,
    private projectService: ProjectService, private userService: UserService, public toastController: ToastController) { }

  ngOnInit() {
    this.storage.get('userId').then((userId) => {
      this.userService.getUserById(userId).subscribe(res => {
        if (res) {
          this.user = res;
          this.isLoading = false;
        }
        if(this.user.project.name) {
          this.edit = false;
        }
      }, err => console.log(err));
    });

    this.polesSubscription = this.poleService.getPoles().subscribe(poles => {
      this.poles = poles;    
    })
    this.projectsSubscription = this.projectService.getProjects().subscribe(projects => {
      this.projects = projects
    })
  }

  ngOnDestroy(): void {
    this.polesSubscription.unsubscribe();
    this.projectsSubscription.unsubscribe();
  }

  logout() {
    this.storage.remove("userId");
    this.router.navigate(['/'])
  }

  filterProjects(ev: any) {
    
    let val = ev.target.value;

    if (val == ""){
      this.projectsFilter = [];
    } else {
      if (val && val.trim() !== '') {
      this.projectsFilter = this.projects.filter(function(project) {
        return project.name.toLowerCase().startsWith(val.toLowerCase());
      });
      }
    }
  }

  save() {
    this.userService.updateUser(this.user).subscribe(
      res => {
        console.log(JSON.parse(res));
        this.presentToast('Modifications enregistées', 'success');
        this.edit = false;
      },
      err => {
        console.log(err);
        
      });
    }
  
  editProject() {
    this.edit = true;
  }

  selectProject(project) { 
    this.user.project = project;
    this.projectsFilter = [];
  }

  async presentToast(message, status) {
    const toast = await this.toastController.create({
      message: message,
      duration: 3000,
      position: "top",
      color: status
    });
    toast.present();
  }
}
