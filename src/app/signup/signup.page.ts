import { Component, OnInit } from '@angular/core';
import { User } from '../models/user';
import { Subscription } from 'rxjs';
import { Pole } from '../models/pole';
import { Role } from '../models/role';
import { UserService } from '../services/user.service';
import { PoleService } from '../services/pole.service';
import { RoleService } from '../services/role.service';
import { ProjectService } from '../services/project.service';
import { Project } from '../models/project';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.page.html',
  styleUrls: ['./signup.page.scss'],
})
export class SignupPage implements OnInit {

  user: User;

  searchUser = "";
  poleLeader = false;

  polesSubscription: Subscription;
  rolesSubscription: Subscription;
  projectsSubscription: Subscription;

  poles: Pole[];
  roles: Role[];
  users: User[];
  projects: Project[];

  usersFilter: User[];
  projectsFilter: Project[];

  constructor(private userService: UserService, private poleService: PoleService, 
    private roleService: RoleService, private projectService: ProjectService) {
    this.user = new User();
  }

  ngOnInit(): void {
    this.polesSubscription = this.poleService.getPoles().subscribe(poles => {
      this.poles = poles;    
    })
    this.rolesSubscription = this.roleService.getRoles().subscribe(roles => {
      this.roles = roles;
    })
    this.projectsSubscription = this.projectService.getProjects().subscribe(projects => {
      this.projects = projects
    })
  }

  ngOnDestroy(): void {
    this.polesSubscription.unsubscribe();
    this.rolesSubscription.unsubscribe();
    this.projectsSubscription.unsubscribe();
  }

  save() {
    this.userService.createUser(this.user).subscribe(
      res => console.log(res),
      err => console.log(err)
    )
  }

  filterUsers(ev: any) {
    
    let val = ev.target.value;

    if (val == ""){
      this.usersFilter = [];
    } else {
      if (val && val.trim() !== '') {
      this.usersFilter = this.users.filter(function(user) {
        return user.email.toLowerCase().startsWith(val.toLowerCase());
      });
      }
    }
  }

  filterProjects(ev: any) {
    
    let val = ev.target.value;

    if (val == ""){
      this.projectsFilter = [];
    } else {
      if (val && val.trim() !== '') {
      this.projectsFilter = this.projects.filter(function(project) {
        return project.name.toLowerCase().startsWith(val.toLowerCase());
      });
      }
    }
  }

  selectProject(project) { 
    this.user.project = project;
    this.projectsFilter = [];
  }

  onChangeRole(roleId) {
    if (roleId === 1) {
      this.poleLeader = true;
    } else {
      this.poleLeader = false;
    }
  }
}
