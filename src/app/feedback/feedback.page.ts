import { Component, OnInit } from '@angular/core';
import { FeedbackService } from '../services/feedback.service';
import { Feedback } from '../models/feedback';
import { ToastController, ModalController } from '@ionic/angular';
import { trigger, state, style, animate, transition } from '@angular/animations';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-feedback',
  templateUrl: './feedback.page.html',
  styleUrls: ['./feedback.page.scss'],
  animations: [
    trigger('changeState', [
      state('inactive', style({
        transform: 'scale(1)' ,
        opacity: '0.6'
      })),
      state('active', style({
        transform: 'scale(1.3)',
        opacity: '1'
      })),
      transition('active => inactive', animate('200ms')),
      transition('inactive => active', animate('300ms'))
    ])
  ]
})
export class FeedbackPage implements OnInit {

  feedback: Feedback;
  state: string = 'inactive';

  smiley = false;
  feedbacks: Feedback[] = [];

  subscription: Subscription;

  smileys: any[] = [
    { 'feeling': 'angry', 'img': '../../assets/images/angry.svg', 'state' : 'inactive'},
    { 'feeling': 'sad', 'img': '../../assets/images/sad.svg', 'state' : 'inactive'},
    { 'feeling': 'confused', 'img': '../../assets/images/confused.svg', 'state' : 'inactive'},
    { 'feeling': 'happy', 'img': '../../assets/images/happy.svg','state' : 'inactive'},
    { 'feeling': 'in-love', 'img': '../../assets/images/in-love.svg', 'state' : 'inactive'}
  ];
  
  constructor(private modalController: ModalController, public toastController: ToastController, private feedbackService: FeedbackService) { }

  ngOnInit() {
    this.feedback = new Feedback();
    this.feedbackService.getFeedbacks().subscribe(feedbacks => {
      this.feedbacks = feedbacks;
      console.log(this.feedbacks);
    });
  }

  saveFeedback() {
    this.feedbackService.createFeedback(this.feedback).subscribe(res => {
      console.log(res);
      this.feedbacks.push(this.feedback);
      this.feedbackService.sendFeedback(this.feedbacks);  
    });
    this.close();
  }

  close() {
    this.modalController.dismiss();
  }
    
  changeState(smiley) {
    this.smiley = true;
    this.smileys.forEach(this.resetSmileys)
    smiley.state = smiley.state === 'inactive' ? 'active' : 'inactive' ;
    this.feedback.feeling = smiley.img;
  }

  resetSmileys(s) {
    return s.state = 'inactive';
  }

  async presentToast(msg: string) {
    const toast = await this.toastController.create({
      message: msg,
      showCloseButton: true,
      position: 'top',
      closeButtonText: 'OK'
    });
    toast.present();
  }
}
