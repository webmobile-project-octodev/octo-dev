import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { PROJECT_API } from 'src/config';
import { Project } from '../models/project';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProjectService {

  constructor(private httpClient: HttpClient) { }

  createProject(project: Project) {
    return this.httpClient.post(PROJECT_API + '/create', project, {responseType: 'text'})
    .subscribe(
      res => console.log(res),
  )}

  getProjects(): Observable<Project[]> {
    return this.httpClient.get<Project[]>(PROJECT_API + '/all');
  }

  getProjectsByPole(pole_id: number): Observable<Project[]> {
    return this.httpClient.get<Project[]>(PROJECT_API + '/pole/' + pole_id);
  }
}

