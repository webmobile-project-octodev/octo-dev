import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FEEDBACK_API } from 'src/config';
import { Feedback } from '../models/feedback';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FeedbackService {

  constructor(private httpClient: HttpClient) { }

  private subject = new Subject<any>();

  sendFeedback(feedback: Feedback[]) {
      this.subject.next(feedback);
  }

  getFeedbacksSubject(): Observable<any> {
      return this.subject.asObservable();
  }

  createFeedback(feedback: Feedback) {
    return this.httpClient.post(FEEDBACK_API + '/create', feedback, {responseType: 'text'});
  }

  getFeedbacks(): Observable<Feedback[]> {
    return this.httpClient.get<Feedback[]>(FEEDBACK_API + '/all');
  }
}