import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { POLE_API } from 'src/config';
import { Pole } from '../models/pole';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PoleService {

  constructor(private httpClient: HttpClient) { }

  createPole(pole: Pole) {
    return this.httpClient.post(POLE_API + '/create', pole, {responseType: 'text'})
    .subscribe(
      res => console.log(res),
  )}

  getPoles(): Observable<Pole[]> {
    return this.httpClient.get<Pole[]>(POLE_API + '/all');
  }

  getPoleById(id): Observable<Pole> {
    return this.httpClient.get<Pole>(POLE_API + '/'+id);
  }
}

