import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ROLE_API } from 'src/config';
import { Role } from '../models/role';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RoleService {

  constructor(private httpClient: HttpClient) { }

  getRoles(): Observable<Role[]> {
    return this.httpClient.get<Role[]>(ROLE_API + '/all');
  }
}

