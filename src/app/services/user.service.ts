import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { USER_API } from 'src/config';
import { User } from '../models/user';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private httpClient: HttpClient) { }

  getUsers(): Observable<any> {
    return this.httpClient.get(USER_API + '/all');
  }

  getUserById(id): Observable<any> {
    return this.httpClient.get(USER_API + '/' + id);
  }

  createUser(user: User): Observable<any> {
    return this.httpClient.post(USER_API + '/create', user, {responseType: 'text'});
  }

  updateUser(user: User): Observable<any> {
    return this.httpClient.put(USER_API + '/'+ user._id +'/update', user, {responseType: 'text'});
  }

  authentication(user: User) {
    return this.httpClient.post(USER_API + '/authentication', user, {responseType: 'text'});
  }
}
