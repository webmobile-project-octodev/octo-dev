import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouteReuseStrategy } from '@angular/router';
import { IonicStorageModule } from '@ionic/storage';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { UserService } from './services/user.service';
import { FeedbackService } from './services/feedback.service';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { ModalPagePageModule } from './modal-page/modal-page.module';
import { PoleService } from './services/pole.service';
import { RoleService } from './services/role.service';
import { SignupPageModule } from './signup/signup.module';
import { ProjectDetailPageModule } from './project-detail/project-detail.module';
import { RegisterPageModule } from './register/register.module';
import { ProfilPageModule } from './profil/profil.module';
import { FeedbackPageModule } from './feedback/feedback.module';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import { QrcodePageModule } from './qrcode/qrcode.module';


@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [BrowserModule, BrowserAnimationsModule, IonicModule.forRoot(), IonicStorageModule.forRoot(), AppRoutingModule, HttpClientModule, FormsModule, 
    ModalPagePageModule, SignupPageModule, ProjectDetailPageModule, RegisterPageModule, ProfilPageModule, FeedbackPageModule, QrcodePageModule],
  providers: [
    UserService,
    FeedbackService,
    PoleService,
    RoleService,
    StatusBar,
    SplashScreen,
    BarcodeScanner,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
