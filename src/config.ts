const LOCAL = "http://127.0.0.1:3001";
// const LOCAL = "http://192.168.43.17:3001";
export const USER_API = LOCAL + "/user";
export const FEEDBACK_API = LOCAL + "/feedback";
export const POLE_API = LOCAL + "/pole";
export const ROLE_API = LOCAL + "/role";
export const PROJECT_API = LOCAL + "/project";