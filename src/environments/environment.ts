// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyDVyaPsp2K0E-BB7jcgt67BCoio3-9UxWM",
    authDomain: "ionicproject-6db60.firebaseapp.com",
    databaseURL: "https://ionicproject-6db60.firebaseio.com",
    projectId: "ionicproject-6db60",
    storageBucket: "ionicproject-6db60.appspot.com",
    messagingSenderId: "980069109514"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
